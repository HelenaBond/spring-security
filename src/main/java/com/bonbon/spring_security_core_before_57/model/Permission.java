package com.bonbon.spring_security_core_before_57.model;

public enum Permission {
    PERMISSION_READ("permission:read"),
    PERMISSION_WRITE("permission:write");

    private final String permission;

    Permission(String permission) {
        this.permission = permission;
    }

}
