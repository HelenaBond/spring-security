package com.bonbon.spring_security_core_before_57.model;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

public enum Roles {
    USER(Set.of(Permission.PERMISSION_READ)),
    ADMIN(Set.of(Permission.PERMISSION_READ, Permission.PERMISSION_WRITE));

    private final Set<Permission> permissions;

    Roles(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    //для интеграции нашего класса Roles в класс реализацию GrantedAuthority, которое является полем класса User из SpringSecurity
    public Set<SimpleGrantedAuthority> getAuthorities() {
        return getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.name()))
                .collect(Collectors.toSet());
    }
}
