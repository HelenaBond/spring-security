package com.bonbon.spring_security_core_before_57.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Developer {
    private Long id;
    private String firstName;
    private String lastName;
}
