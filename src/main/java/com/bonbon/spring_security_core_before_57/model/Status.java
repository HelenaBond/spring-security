package com.bonbon.spring_security_core_before_57.model;

public enum Status {
    ACTIVE,
    BANNED
}
