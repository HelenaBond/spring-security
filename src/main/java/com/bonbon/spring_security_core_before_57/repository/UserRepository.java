package com.bonbon.spring_security_core_before_57.repository;

import com.bonbon.spring_security_core_before_57.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);
}
