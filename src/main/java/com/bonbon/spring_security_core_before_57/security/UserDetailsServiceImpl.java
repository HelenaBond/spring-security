package com.bonbon.spring_security_core_before_57.security;

import com.bonbon.spring_security_core_before_57.model.Status;
import com.bonbon.spring_security_core_before_57.model.User;
import com.bonbon.spring_security_core_before_57.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("userDetailsServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email).orElseThrow(() ->
                new UsernameNotFoundException("User doesn't exist"));
        return fromUser(user);
    }

    private UserDetails fromUser(User user) {
        return new org.springframework.security.core.userdetails.User(
                user.getEmail(), user.getPassword(),
                //Работает из коробки.
                //Если хоть одно значение будет false, то пользователь не сможет авторизоваться.
                //Удобно настраивать бан через базу.
                //Есть конструктор без этих аргументов.
                //Но лучше иметь возможность отсечь пользователя в любой момент.
                user.getStatus().equals(Status.ACTIVE), //заглушка
                user.getStatus().equals(Status.ACTIVE), //заглушка
                user.getStatus().equals(Status.ACTIVE), //заглушка
                user.getStatus().equals(Status.ACTIVE), //пока реализовано только это значение
                user.getRole().getAuthorities()
        );
    }
}
